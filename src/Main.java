import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        /*File file = new File("/home/ani/Downloads/new5out.txt");
//        List<String> b = getQuotesExpressions(file);
        for (String m : b) {
            System.out.println(m);
        }*/
        getQuotesExpressions("/home/ani/Downloads/new5.txt", "/home/ani/Downloads/new5out.txt");
    }

    static void getQuotesExpressions(String pathSource, String outFile) throws IOException {
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            reader = new BufferedReader(new FileReader(pathSource));
            writer = new BufferedWriter(new FileWriter(outFile));
            String line = reader.readLine();
            while (line != null) {
                int index = 0;
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == '"') {
                        StringBuilder string = new StringBuilder();
                        index = i + 1;

                        while (line.charAt(index) != '"' && index != line.length() - 1) {

                            string.append(line.charAt(index));
                            index++;
                        }
                        string.append("\n");
                        writer.write(string.toString());
                        i = index;
                        index = 0;
                    }
                    if (line.charAt(i) == '«') {
                        StringBuilder string = new StringBuilder();
                        index = i + 1;

                        while (line.charAt(index) != '»' && index != line.length() - 1) {
                            string.append(line.charAt(index));
                            index++;
                        }
                        string.append("\n");
                        writer.write(string.toString());
                        i = index;
                        index = 0;
                    }

                }
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            reader.close();
            writer.close();
        }
    }
}
